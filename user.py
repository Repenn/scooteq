import sqlite3
from datetime import datetime

PRICE_PER_MINUTE = 0.19

conn = sqlite3.connect("sqlite.db")
cursor = conn.cursor()

# calculate the price of a ride based on the start and the end time
def calculate_price(start_time, end_time):
    return int((end_time - start_time).total_seconds() / 60) * PRICE_PER_MINUTE


class User:
    def __init__(self, username, password, first_name, last_name, rides_count, user_payment, renting):
        self.username = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name
        self.rides_count = rides_count
        self.user_payment = user_payment
        self.renting = renting

    # create a new user in the db or update it if it allready exists
    def save(self):
        if cursor.execute(f"SELECT * FROM users WHERE username = '{self.username}';").fetchone():
            cursor.execute(f"UPDATE users set username = '{self.username}', user_password = '{self.password}', first_name = '{self.first_name}', last_name = '{self.last_name}', rides_count = '{self.rides_count}', user_payment = '{self.user_payment}', renting = '{self.renting}' WHERE username = '{self.username}';")
        else:
            cursor.execute(
                f"INSERT INTO users (username, user_password, first_name, last_name, rides_count, user_payment, renting) VALUES ('{self.username}', '{self.password}', '{self.first_name}', '{self.last_name}', '{self.rides_count}', '{self.user_payment}', '{self.renting}');"
        )
        conn.commit()

    # set the flag for logged in
    def log_in(self):
        cursor.execute(f"UPDATE logging SET username = '{self.username}' WHERE id = 0;")
        conn.commit()

    # remove the flag for logged in
    def log_out(self):
        cursor.execute(f"UPDATE logging SET username = null WHERE id = 0;")
        conn.commit()

    # save a time stamp associated to a user and a scooter
    # mark the specific scooter as in use
    # mark the user as currently renting
    def start_ride (self, scooter_id):
        start_time = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M')
        cursor.execute(f"INSERT INTO rentals (username, scooter_id, rental_start) VALUES ('{self.username}', '{scooter_id}', '{start_time}');")
        cursor.execute(f"UPDATE users SET renting = {cursor.lastrowid} WHERE username = '{self.username}';")
        cursor.execute(f"UPDATE scooters SET is_in_use = 1 WHERE id = {scooter_id};")
        conn.commit()

    # associate a new ime stamp to the first one
    # and remove the marks
    def end_ride (self):
        end_time = datetime.now().replace(second=0, microsecond=0)
        rental_id = cursor.execute(f"SELECT renting FROM users WHERE username = '{self.username}';").fetchone()[0]
        start_time = datetime.strptime(cursor.execute(f"SELECT rental_start FROM rentals WHERE rental_id = '{rental_id}';").fetchone()[0], '%Y-%m-%d %H:%M')
        scooter_id = cursor.execute(f"SELECT scooter_id FROM rentals WHERE rental_id = '{rental_id}';").fetchone()[0]
        cursor.execute(f"UPDATE rentals SET rental_end = '{end_time}';")
        cursor.execute(f"UPDATE scooters SET is_in_use = 0 WHERE id = {scooter_id};")
        cursor.execute(f"UPDATE users SET renting = 0;")
        price = calculate_price(start_time, end_time)
        cursor.execute(f"UPDATE rentals SET rental_price = '{price}' WHERE rental_id = {rental_id};")
        conn.commit()