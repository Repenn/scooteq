from mockito import *
from datetime import datetime
import user
import app


# test the calculation of the price
def test_calculate_price():
    start = datetime(2022, 6, 30, 15, 3)
    end = datetime(2022, 6, 30, 15, 19)
    assert user.calculate_price(start, end) == 3.04


# First of all: this doesn't work.
# Regarding to the requirements we need at least two tests,
# but there is only one single method which neither uses the database nor the GUI.
# So this is the (unfortunatly unsuccessful) attempt of mocking the database.
def test_get_user():
    connection = mock(app.conn)
    cursor = mock(app.cursor)
    result = "John"

    when(app.sqlite3).connect('sqlite.db').thenReturn(connection)
    when(connection).cursor().thenReturn(cursor)
    when(cursor).fetchone().thenReturn(result)

    assert app.get_user('stephie') == 'John'


