import tkinter as tk
from functools import partial
import sqlite3
import app
import user

conn = sqlite3.connect("sqlite.db")
cursor = conn.cursor()
quit_program = False


# reopen the mainwindow every time untill the quit butten is pressed
while not quit_program:
    root = tk.Tk()
    root.title("Welcome to ScooTeq")


    # quit the program
    def quit():
        root.destroy()
        global quit_program
        quit_program = True


    # a general message box that takes title and text to display
    def message_box(message_title, message_text):
        message_window = tk.Tk()
        message_window.title(message_title)
        tk.Label(message_window, text=message_text, font=('Helvetica',10)).pack(pady=20, padx=50)
        tk.Button(message_window, text="Ok", font=('Helvetica bold', 10),command=message_window.destroy).pack(pady=10, padx=50)


    # check username and password and log the user in if correct
    def log_in(username, password, window):
        user1 = app.get_user(username.get())
        
        if user1 == None:
            message_box("False credentials", app.FALSE_CREDENTIALS)
        else:
            pw_actual = cursor.execute(f"SELECT user_password FROM users WHERE username = '{username.get()}';").fetchone()[0]

            if password.get() != pw_actual:
                message_box("False credentials", app.FALSE_CREDENTIALS)
            else:
                user1.log_in()
                window.destroy()
                

    # request username and password from user to log in
    def login_view():
        root.destroy()
        login_window = tk.Tk() 
        login_window.title("Log In")
        login_window.geometry("250x220")

        tk.Label(login_window, text="Enter your Username", font=('Helvetica',10)).pack(pady=20)
        username= tk.Entry(login_window, width=20)
        username.pack()

        tk.Label(login_window, text="Enter your Password", font=('Helvetica',10)).pack(pady=20)
        password= tk.Entry(login_window, show="*",width=20)
        password.pack()

        login = partial(log_in, username, password, login_window)
        tk.Button(login_window, text="Submit", font=('Helvetica bold', 10),command=partial(login)).pack(pady=10)


    # create user and account and give success message
    def account_created(username, password, first_name, last_name, payment, create_account_window):
        rides_count = 0
        renting = 0
        new_user = user.User(username.get(), password.get(), first_name.get(), last_name.get(), rides_count, payment.get(), renting)
        new_user.save()
        conn.commit()

        create_account_window.destroy()
        message_box("Account Created", app.ACCOUNT_SUCCESS)


    # request all necessary data for user creation
    def create_account():
        root.destroy()
        create_account_window = tk.Tk() 
        create_account_window.title("Create Account")    
        create_account_window.geometry("250x450")

        tk.Label(create_account_window, text="Enter your username", font=('Helvetica',10)).pack(pady=20)
        username = tk.Entry(create_account_window,width=20)
        username.pack()

        tk.Label(create_account_window, text="Enter the Password", font=('Helvetica',10)).pack(pady=20)
        password= tk.Entry(create_account_window, show="*",width=20)
        password.pack()
        
        tk.Label(create_account_window, text="Enter your first name", font=('Helvetica',10)).pack(pady=20)
        first_name = tk.Entry(create_account_window, width=20)
        first_name.pack()

        tk.Label(create_account_window, text="Enter your last name", font=('Helvetica',10)).pack(pady=20)
        last_name = tk.Entry(create_account_window, width=20)
        last_name.pack()

        tk.Label(create_account_window, text="Enter your payment method", font=('Helvetica',10)).pack(pady=20)
        payment = tk.Entry(create_account_window, width=20)
        payment.pack()

        created = partial(account_created, username, password, first_name, last_name, payment, create_account_window)
        tk.Button(create_account_window, text="Submit", font=('Helvetica bold', 10),command=created).pack(pady=10)


    # start ride
    def start_ride(id):
        app.get_logged_in_user().start_ride(id.get())
        root.destroy()


    # end ride
    def end_ride():
        rental_id = cursor.execute(f"SELECT renting FROM users WHERE username = '{app.get_logged_in_user().username}';").fetchone()[0]
        app.get_logged_in_user().end_ride()
        rental_price = round(cursor.execute(f"SELECT rental_price FROM rentals WHERE rental_id = {rental_id};").fetchone()[0], 2)
        root.destroy()
        
        message_box("End ride", app.END_RIDE + f"\nThe price for this ride is {rental_price} €")


    # display the total cost of all rides
    def check_balance():
        root.destroy()
        message_box('Balance', app.get_balance())


    # log out the user
    def log_out():
        app.get_logged_in_user().log_out()
        root.destroy()


    USER = app.get_logged_in_user()

    # fill the main window with content for not logged in users
    if  USER == None:
        tk.Label(root, text="Welcome to ScooTeq!", font=('Helvetica',20)).pack(pady=5, padx=50)
        tk.Label(root, text="What do you want to do?", font=('Helvetica',15)).pack(pady=5, padx=50)

        tk.Button(root, text="Log In", font=('Helvetica bold', 10),command=login_view).pack(pady=10)
        tk.Button(root, text="Create Account", font=('Helvetica bold', 10),command=create_account).pack(pady=10)
        tk.Button(root, text="Quit", font=('Helvetica bold', 10),command=quit).pack(pady=10)

    # ... and for a specific logged in user
    else:
        # display end ride button if user is currently riding
        if USER.renting > 0:
            tk.Button(root, text="End Ride", font=('Helvetica bold', 10),command=end_ride).grid(row=0, column=1, pady=10, padx=50)
        # otherwise display start ride button
        else:
            tk.Label(root, text="with scooter", font=('Helvetica',10)).grid(row=0, column=1, pady=10, padx=50)
            all_scooter_ids = app.get_available_scooters()
            first_id = tk.StringVar(root)
            first_id.set(all_scooter_ids[0])
            scooter_id = tk.OptionMenu(root, first_id, *all_scooter_ids).grid(row=0, column=2, pady=10, padx=50)
            start_ride = partial(start_ride, first_id)
            tk.Button(root, text="Start Ride", font=('Helvetica bold', 10),command=partial(start_ride)).grid(row=0, column=0, pady=10, padx=50)

        tk.Button(root, text="Check Ballance", font=('Helvetica bold', 10),command=partial(check_balance)).grid(row=1, column=1, pady=10, padx=50)
        tk.Button(root, text="Log Out", font=('Helvetica bold', 10),command=log_out).grid(row=2, column=1, pady=10, padx=50)
        tk.Button(root, text="Quit", font=('Helvetica bold', 10),command=quit).grid(row=3, column=1, pady=10, padx=50)

    root.mainloop()