DROP TABLE IF EXISTS users;
CREATE TABLE users (
    username text PRIMARY KEY,
    user_password text,
    first_name text,
    last_name text,
    rides_count text,
    user_payment text,
    renting integer
);

DROP TABLE IF EXISTS scooters;
CREATE TABLE scooters (
    id integer PRIMARY KEY,
    is_in_use integer,
    battery integer
);

DROP TABLE IF EXISTS rentals;
CREATE TABLE rentals (
    rental_id integer PRIMARY KEY AUTOINCREMENT,
    username text,
    scooter_id integer,
    rental_start datetime,
    rental_end datetime,
    rental_price real,
    FOREIGN KEY(username) REFERENCES users(username),
    FOREIGN KEY(scooter_id) REFERENCES scooters(id)
);

DROP TABLE IF EXISTS logging;
CREATE TABLE logging (
    id INTEGER PRIMARY KEY CHECK (id = 0),
    username text
);

INSERT INTO users
VALUES
    ("stephie", "password", "Stephanie", "Omar", "", "DE49 1233 3456 2345 33", 0),
    ("frepenn", "password", "Florian", "Repenn", "", "DE555", 0);

INSERT INTO scooters
VALUES
    (1, False, 100),
    (2, False, 100),
    (3, False, 100),
    (4, False, 100),
    (5, False, 100),
    (6, False, 100),
    (7, False, 100),
    (8, False, 100),
    (9, False, 100),
    (10, False, 100);
    
INSERT INTO rentals (username, scooter_id, rental_start, rental_end, rental_price)
VALUES
    ("stephie", 1, "2022-06-27 21:24", "2022-06-27 21:26", 0.38),
    ("stephie", 8, "2022-07-01 05:03", "2022-07-01 05:21", 3.42),
    ("stephie", 2, "2022-07-03 18:06", "2022-07-03 18:31", 4.75);

INSERT INTO logging
VALUES
    (0, null);