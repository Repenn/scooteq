import getpass
import sqlite3
import user


conn = sqlite3.connect("sqlite.db")
cursor = conn.cursor()

WELCOME = "\nWelcome to ScooTeq! What do you want to do?\n\n"\
    "[L]og in\n"\
    "[C]reate an account\n"\
    "[Q]uit\n"

WELCOME_USER = "\nHello {}! What do you want to do?\n\n"\
    "[S]tart a ride\n"\
    "[E]nd a ride\n"\
    "Check [b]alance\n"\
    "Log [o]ut\n"\
    "[Q]uit\n"

LOGOUT = "\nYou have successfully logged out.\n"
BYE = "\nHave a nice day!\n"
ENTER_USERNAME = "\nUsername: "
ENTER_NEW_USERNAME = "\nUsername (min. 5 characters): "
ENTER_PASSWORD = "Password: "
ENTER_NEW_PASSWORD = "Password (min. 5 characters): "
FALSE_CREDENTIALS = "\nSorry, the combination of username and password you entered does not exist"
CREATE_ACCOUNT = "\nPlease follow the instructions to create an account."
USERNAME_NOT_AVAILABLE = "\nSorry, the username you chose does allready exist. Please try again."
FIRST_NAME = "\nPlease enter your first name: "
LAST_NAME = "\nPlease enter your last name: "
USER_PAYMENT = "\nPlease enter your IBAN: "
ACCOUNT_SUCCESS = "\nYour data was saved successfully. Now you can log in with your new account."
NO_VALID_CHOICE = "\nPlease try again and select a valid choice."
DISPLAY_BALANCE = "\nThe total bill for all of your rides is {} €"
SELECT_SCOOTER = "\nPlease select a scooter.\nScooter near you:\n"
SCOOTER_ID = "\nScooter id: "
START_RIDE = "\nThank you for choosing scooteq. Have a nice ride!"
END_RIDE = "\nYou successfully parked the scooter. Have a nice day."


# create the database and fill it with example values
def initialize_db():
    sql_file = open("script.sql")
    sql_as_string = sql_file.read()
    cursor.executescript(sql_as_string)
    conn.commit()


# get a specific user from username
def get_user(username):
    requested_user = cursor.execute(f"SELECT * FROM users WHERE username = '{username}';").fetchone()
    try:
        return user.User(requested_user[0], requested_user[1], requested_user[2], requested_user[3], requested_user[4], requested_user[5], requested_user[6])
    except:
        return None


# return the currently logged in user or None
def get_logged_in_user():
    username = cursor.execute("SELECT username FROM logging;").fetchone()[0]
    if not username == None:
        return get_user(username)
    else:
        return None


# flag a user as logged in
def log_in():
    username = input(ENTER_USERNAME)
    pw_entered = getpass.getpass(ENTER_PASSWORD)
    user = get_user(username)
    
    if user == None:
        print(FALSE_CREDENTIALS)
    else:
        pw_actual = cursor.execute(f"SELECT user_password FROM users WHERE username = '{username}'").fetchone()[0]

        if pw_entered != pw_actual:
            print(FALSE_CREDENTIALS)
        else:
            user.log_in()


# return a list of all scooter ids which are not in use
def get_available_scooters():
    available_scooters = []
    cursor.execute("SELECT id FROM scooters WHERE is_in_use = 0")

    for row in cursor:
        available_scooters.append(str(row[0]))

    return available_scooters


# request necessary information to create a new user and save it to db
def create_account():
    print(CREATE_ACCOUNT)
    username = ""

    while len(username) < 5:
        username = input(ENTER_NEW_USERNAME)

        if cursor.execute(f"SELECT username FROM users WHERE username = '{username}';").fetchone() is not None:
            print(USERNAME_NOT_AVAILABLE)
            username = ""

    password = ""
    while len(password) < 5:
        password = input(ENTER_NEW_PASSWORD)

    first_name = input(FIRST_NAME)
    last_name = input(LAST_NAME)
    rides_count = 0
    user_payment = input(USER_PAYMENT)
    renting = 0

    new_user = user.User(username, password, first_name, last_name, rides_count, user_payment, renting)
    new_user.save()
    conn.commit()
    print(ACCOUNT_SUCCESS)


# calculate the total amount of costs of all past rides
def get_balance():
    balance = 0.0

    for row in cursor.execute(f"SELECT rental_price FROM rentals WHERE username = '{get_logged_in_user().username}'"):
        balance += float(row[0])

    return DISPLAY_BALANCE.format(round(balance, 2))


# simulate the start of a ride
def start_ride():
    print(SELECT_SCOOTER)
    print(', '.join(get_available_scooters()))
    selected_scooter = ""

    while selected_scooter not in get_available_scooters():
        selected_scooter = input(SCOOTER_ID)

    get_logged_in_user().start_ride(selected_scooter)
    print(START_RIDE)


# kinda obvious, but end the ride
def end_ride():
    get_logged_in_user().end_ride()
    print(END_RIDE)


# display a user interface where the user can manage all functionality
def user_interaction():
    logged_in = False

    if get_logged_in_user() == None:
        action = input(WELCOME).lower()
    else:
        action = input(WELCOME_USER.format(get_logged_in_user().first_name)).lower()
        logged_in = True

    if action == 'l' and not logged_in:
        log_in()
        user_interaction()
    elif action == 'c' and not logged_in:
        create_account()
        user_interaction()
    elif action == 'b' and logged_in:
        print(get_balance())
        user_interaction()
    elif action == 's' and logged_in and get_logged_in_user().renting == 0:
        start_ride()
        user_interaction()
    elif action == 'e' and logged_in and get_logged_in_user().renting > 0:
        end_ride()
        user_interaction()
    elif action == 'o' and logged_in:
        get_logged_in_user().log_out()
        print(LOGOUT)
        user_interaction()
    elif action == 'q':
        print(BYE)
    else:
        print(NO_VALID_CHOICE)
        user_interaction()

if __name__ == "__main__":
    user_interaction()


