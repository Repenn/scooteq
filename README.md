# scooteq

## Getting started

### Prerequirements:

* Clone project
* Run pipenv shell (if you don't use python 3.10, change the required version in the pipfile to your version)
* Run pipenv install (depending on your python version it may be necessary to install tkinter globaly)
* Initialize the db by running utilities.py (sqlite, no external db required)
* To run the tests just enter 'pytest' in the console

### Navigate through conole program:

* Run app.py
* At any time you can quit the program by typing 'q' and return
* Your options after starting the first time are:
  * log in
  * create account
  * quit

* You can log in with 'stephie' and 'password' or create your own account
* After logging in your options are:
  * start ride      (simulates a ride with a scooter by marking the user and the chosen scooter and saves a time stamp)
  * end ride        (takes back changes from start ride and saves another time stamp and calculates the costs of the ride)
  * check balance   (calculates the costs of all rides from the past)
  * log out
  * quit

* Everything is persistent. For example you can log in and start a ride, than terminate the program and after restarting it <br/>
  you will still be logged in and can end the ride. Switching to another user and back is also possible.

### Navigate through GUI:

* Run gui_tkinter.pyw to start the GUI
* The quit button is the only way to end the program (except of killing the terminal)
* The functionality is the same as in the console program

### Notes (what could be better):

* The user information is saved as plane text and is not hashed <br/>
  (for production environment I would recommend a framework like django which comes with user management, instead of reinventing the wheel)
* It is not possible to log in more than one user at the same time (same solution like above)
* Not enough tests: There should be at least one unit test for each method
* Redundancy: The methods should be more general, so you could use them for the GUI and the console programe aswell
* Input sanitation: There should be more checks for user input
* The mocking in the second test doesn't work